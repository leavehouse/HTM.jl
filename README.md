# HTM.jl

An implementation of Hierarchical Temporal Memory in Julia.

# Install

This is not ready to be used, you shouldn't do this:

```julia-repl
julia> Pkg.clone("git@gitlab.com:leavehouse/HTM.jl.git")
```

# Temporal Memory Pseudocode

```
fn compute!(active_columns, learn)
  for each column c
    if c in active_columns
      activate_column!(c)
    else
      if learn
        // punish column c
        for seg in {matching segments in column c}
           adapt_segment!(seg, -INACTIVE_MATCHING_SEG_DEC, 0.)
        end
      end
    end
  end

  compute_synaptic_activity!()
end

fn activate_column!(c)
  if c has predicting distal segments
    // activate predicted column c
    for seg in {predicting segments in column c}
      make the postsynaptic cell of seg both active and winning
      if learn
        adapt_and_grow_segment!(seg)
      end
    end
  else
    // activate unpredicted column c
    make all cells in c active // "burst" the column c
    if there are any matching distal segments in c
      winner_seg = the matching segment with largest presynaptic activity
      winner = the postsynaptic cell of winner_seg
      if learn
        adapt_and_grow_segment!(winner_seg)
      end
    else
      winner = the cell in c with the fewest number of segments
      if learn
        if there are some previous winner cells
          new_seg = grow a segment on winner
          try to grow up to MAX_NEW_SYN_COUNT synapses on new_seg
        end
      end
    end
    make winner a winning cell
  end
end

fn compute_synaptic_activity!()
  for each segment, tally up the number of active pre-synaptic cells that:
   1) project to potential synapses in the segment
   2) project to connected synapses in the segment
  The first number is called the *presynaptic activity* of the segment, while
  the second is the *postsynaptic activity*.

  These values can be used to construct two lists:
   1) the predicting segments, whose postsyn activity >= SEG_PREDICT_THRESH
   2) the matching segments, whose presyn activity >= SEG_MATCH_THRESH
end


fn adapt_segment(segment, perm_inc, perm_dec)
  for syn in segment.synapses
    perm_delta = if syn.presyn_cell was previously active
      perm_inc
    else
      -perm_dec
    end

    syn.perm = max(0., min(1., syn.perm + perm_delta))

    if syn.perm < PERM_EPSILON
      mark syn to be destroyed
    end
  end
  destroy previously marked synapses
end

fn adapt_and_grow_segment!(seg)
    adapt_segment!(seg, PERM_INC, PERM_DEC)
    if seg's presynaptic activity < MAX_NEW_SYN_COUNT
        try to grow new synapses (up to a total of MAX_NEW_SYN_COUNT) on seg
    end
end
```

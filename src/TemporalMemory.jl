module TM

using Memento

include("misc.jl")
include("network.jl")

logger = Memento.config("debug", fmt = "[{level}] {msg}")

# tallies up presynaptic and postsynaptic activity for a segment
type SegActivity
    presyn::Int
    postsyn::Int
end
SegActivity() = SegActivity(0, 0)
const SegActivities = Dict{SegID, SegActivity}

type Params
    col_dims::Array{Int, 1}
    cells_per_col::Int

    # active connected synapses must meet or exceed this threshold
    seg_predict_thresh::Int

    # active potential synapses must meet or exceed this threshold
    seg_match_thresh::Int

    # permanence must *exceed* this threshold for synapse to be connected
    conn_perm_thresh::Float64

    perm_inc::Permanence
    perm_dec::Permanence
    max_syns_per_seg::Int
    max_segs_per_cell::Int

    # determines how many synapses to try to grow during learning
    max_new_syn_count::Int

    # The initial permanence of a new synapse grown during learning
    new_syn_init_perm::Permanence

    inactive_matching_seg_dec::Permanence
    perm_epsilon::Float64
end

# TODO: defaults ripped off from nupic, examine?
Params(;
    col_dims=[2048],
    cells_per_col=32,
    seg_predict_thresh=13,
    seg_match_thresh=10,
    conn_perm_thresh=0.5,
    perm_inc=0.1,
    perm_dec=0.1,
    max_syns_per_seg=255,
    max_segs_per_cell=255,
    max_new_syn_count=20,
    new_syn_init_perm=0.21,
    inactive_matching_seg_dec=0.0,
    perm_epsilon=0.00001) = Params(
        col_dims, cells_per_col, seg_predict_thresh, seg_match_thresh,
        conn_perm_thresh, perm_inc, perm_dec, max_syns_per_seg,
        max_segs_per_cell, max_new_syn_count, new_syn_init_perm,
        inactive_matching_seg_dec, perm_epsilon)


function num_columns(params::Params)
    reduce(*, 1, params.col_dims)
end

function num_cells(params::Params)
    num_columns(params) * params.cells_per_col
end

# For a collection of cells, stores the matching and
# predicting segments
type MPSegments
    matching
    predicting
end

type OnCells
    active::Set{CellID}
    winning::Set{CellID}
end

OnCells() = OnCells(Set(), Set())

Base.show(io::IO, cs::OnCells) = print(io, "On{active: ", cs.active, ", winning: ", cs.winning, "}")

type TemporalMemory
    params::Params
    net::RegionNetwork
    on_cells::OnCells
    prev_on_cells::Nullable{OnCells}
    # psegments is always(?) a subset of msegments because
    # seg_match_thresh <= seg_predict_thresh
    psegments::SegIDs # predicting segments
    msegments::SegIDs # matching segments
    seg_activities::SegActivities
    seg_last_predicted::Dict{SegID, Int}
    step_num::Int
end

TemporalMemory(params::Params) = TemporalMemory(
    params, RegionNetwork(num_cells(params)), OnCells(), Nullable(), Set(),
    Set(), Dict(), Dict(), 1)

function num_columns(tm::TemporalMemory)
    num_columns(tm.params)
end

# TODO: does this work with > 1 D?
function get_column_cells(tm::TemporalMemory, c)
    M = tm.params.cells_per_col
    ((c-1)*M + 1):(c*M)
end

# Returns an iterator of all the SegIDs for segments on
# cells in column `col`
function get_column_segs(tm::TemporalMemory, col)
    # First, get all cell ids for cells in col
    col_cell_ids = get_column_cells(tm, col)

    # Map each cell id to its collection of segment IDs
    col_segs = map(cid -> tm.net.cells[cid].segments,
                            col_cell_ids)
    # Flattening gives iterator of all SegIDs of segments on
    # cells in the column
    Iterators.flatten(col_segs)
end

# Returns, among all the segments on column `col`, which
# are matching, and which are predicting
# TODO more efficient than this
function get_column_mp_segs(tm::TemporalMemory, col)::MPSegments
    col_segs = get_column_segs(tm, col)
    col_match_segs = (s for s in col_segs if s in tm.msegments)
    MPSegments(col_match_segs,
               (s for s in col_match_segs if s in tm.psegments))
end

function get_num_segments(tm::TemporalMemory, cell::CellID)
    get_num_segments(tm.net, cell)
end

function get_num_segments(tm::TemporalMemory)
    get_num_segments(tm.net)
end

function get_predicted_cells(tm::TemporalMemory)
    predicted = Set{CellID}()
    for segid in tm.psegments
        postsyn_cell = get_segment(tm.net, segid).postsyn_cell
        if !(postsyn_cell in predicted)
            push!(predicted, postsyn_cell)
        end
    end
    predicted
end

function get_active_cells(tm::TemporalMemory)
    tm.on_cells.active
end

function get_winning_cells(tm::TemporalMemory)
    tm.on_cells.winning
end

function get_prev_on(tm::TemporalMemory)
    get(tm.prev_on_cells)
end

function create_segment!(tm::TemporalMemory, cid::CellID)::SegID
    debug(logger, "creating segment on cell $(cid)")
    # remove enough of the least recently used segments
    while get_num_segments(tm.net, cid) >= tm.params.max_segs_per_cell
        least_recently_used_seg = dictmin(
            Dict(seg => tm.seg_last_predicted[seg]
                 for seg in tm.net.cells[cid].segments))[1]

        debug(logger, "destroying segment $(least_recently_used_seg)")
        destroy_segment!(tm.net, least_recently_used_seg)
    end

    create_segment!(tm.net, cid)
end

function adapt_segment!(tm::TemporalMemory, seg_id::SegID, prev_active_cells, perm_inc::Permanence, perm_dec::Permanence)
    debug(logger, "adapting segment $(seg_id) w/ Aprev and inc/dec = $(perm_inc) / $(perm_dec))")
    syns_to_destroy = []
    perm_deltas = Dict{SynID, Permanence}()

    # Ensure permanence `p` stays 0 <= p <= 1
    bound_perm(p) = max(0., min(1., p))

    for syn_id in get_segment_synapses(tm.net, seg_id)
        permanence = get_permanence(tm.net, syn_id)
        presyn_cell = get_synapse(tm.net, syn_id).presyn_cell

        perm_delta = if presyn_cell in prev_active_cells
            perm_inc
        else
            -perm_dec
        end
        new_perm = bound_perm(permanence + perm_delta)

        if new_perm < tm.params.perm_epsilon
            push!(syns_to_destroy, syn_id)
        else
            perm_deltas[presyn_cell] = new_perm - permanence
            set_permanence!(tm.net, syn_id, new_perm)
        end
    end
    debug(logger, "perm changes: $(perm_deltas)")

    for syn in syns_to_destroy
        debug(logger, "destroying synapse $(syn)")
        destroy_synapse!(tm.net, syn)
    end

    if get_segment_num_synapses(tm.net, seg_id) == 0
        debug(logger, "destroying segment $(seg_id)")
        destroy_segment!(tm.net, seg_id)
    end
end

# Destroy up to (as many as possible) `num_to_destroy` of the lowest
# permanence synapses, excluding the synapses in `excluding` as possible targets
function destroy_lowest_perm_synapses(tm::TemporalMemory, seg_id::SegID,
                                   num_to_destroy, excluding)
    debug(logger, "destroying $(num_to_destroy) lowest perm synapses on segment $(seg_id), excluding $(excluding)")
    segment_syns = get_segment_synapses(tm.net, seg_id)
    candidates = Set(syn for syn in segment_syns
                     if !(syn.presyn_cell_id in excluding))

    for _ in 1:num_to_destroy
        if length(candidates) == 0
            break
        end

        min_synapse = nothing
        min_perm = Inf
        for syn_id in candidates
            synapse = get_synapse(tm.net, syn_id)
            if perm_exceeds_thresh(tm, synapse.permanence, min_perm)
                min_synapse = syn_id
                min_perm = synapse.permanence
            end
        end

        debug(logger, "destroying synapse $(min_synapse)")
        destroy_synapse!(tm.net, min_synapse)
        delete!(candidates, min_synapse)
    end
end

function grow_new_synapses!(tm::TemporalMemory, seg_id::SegID,
                            num_desired_new::Int, prev_winning_cells)
    debug(logger, "want to grow $(num_desired_new) new synapse(s) on segment $(seg_id) out of Wprev")
    # don't grow new synapses to cells that are already presynaptic
    # to this segment
    candidates = Set(prev_winning_cells)
    for synapse_id in get_segment_synapses(tm.net, seg_id)
        presyn_cell = get_synapse(tm.net, synapse_id).presyn_cell
        if presyn_cell in candidates
            delete!(candidates, presyn_cell)
        end
    end

    num_to_create = min(num_desired_new, length(candidates))
    overrun = (get_segment_num_synapses(tm.net, seg_id)
                + num_to_create - tm.params.max_syns_per_seg)
    if overrun > 0
        destroy_low_perm_synapses!(tm, seg_id, overrun, prev_winning_cells)
    end

    num_to_create = min(num_to_create,
                        (tm.params.max_syns_per_seg
                         - get_segment_num_synapses(tm.net, seg_id)))

    created = Set()
    for _ in 1:num_to_create
        rand_candidate = rand(candidates)
        new_syn_id = create_synapse!(tm.net, seg_id, rand_candidate,
                                     tm.params.new_syn_init_perm)
        delete!(candidates, rand_candidate)
        push!(created, rand_candidate)
    end

    if length(created) >=  div(length(prev_winning_cells), 2)
        pw_not_created = [pw for pw in prev_winning_cells
                          if !(pw in created)]
        if length(pw_not_created) == 0
            debug(logger, "created synapses for all")
        else
            debug(logger, "created synapses for all except $(pw_not_created)")
        end
    else
        if length(created) == 0
            debug(logger, "created no synapses")
        else
            debug(logger, "created synapses for only $(created)")
        end
    end

end

function adapt_and_grow_segment!(tm::TemporalMemory, seg_id::SegID)
    debug(logger, "(adapt_and_grow)ing segment $(seg_id)")

    adapt_segment!(tm, seg_id, get_prev_on(tm).active,
                  tm.params.perm_inc, tm.params.perm_dec)

    # check whether to grow synapses
    presyn_activity = tm.seg_activities[seg_id].presyn
    num_syns_desired = tm.params.max_new_syn_count - presyn_activity

    if num_syns_desired > 0
        grow_new_synapses!(tm, seg_id, num_syns_desired,
                           get_prev_on(tm).winning)
    end
end

# pseudocode
# active = winning = the cells that were predicted
# (predicted = cells with predicting distal segment)
function activate_predicted_column!(tm::TemporalMemory, col, col_predict_segs,
                                    learn::Bool)
    debug(logger, "")
    debug(logger, "activate predicted column=$(col) w/ col_predict_segs = $([seg for seg in col_predict_segs])")
    activated = Set()
    for seg_id in col_predict_segs
        postsyn_cell = get_segment(tm.net, seg_id).postsyn_cell
        if !(postsyn_cell in activated)
            push!(activated, postsyn_cell)
        end

        if learn
            adapt_and_grow_segment!(tm, seg_id)
        end
    end
    activated
end

# pseudocode
# all cells in the column are active
# if there are any matching distal segments
#   winner = the cell with biggest pre-activity for a matching segment
# else
#   winner = the cell with the fewest number of segments
#
# `col_match_segs` is collection of SegIDs?
function activate_unpredicted_column!(tm::TemporalMemory, col, col_match_segs,
                                      learn::Bool)
    debug(logger, "")
    debug(logger, "activate unpredicted column=$(col) w/ col_match_segs = $([seg for seg in col_match_segs])")
    col_cells = get_column_cells(tm, col)

    if !isempty(col_match_segs)
        best_matching_seg = dictmax(Dict(
            seg_id => tm.seg_activities[seg_id].presyn
            for seg_id in col_match_segs))[1]
        winner_cell = get_segment(tm.net, best_matching_seg).postsyn_cell

        if learn
            adapt_and_grow_segment!(tm, best_matching_seg)
        end
    else
        winner_cell = cell_with_fewest_segs(tm.net, col_cells)

        if learn
            prev_winning = get_prev_on(tm).winning
            num_to_grow = min(tm.params.max_new_syn_count,
                              length(prev_winning))

            if num_to_grow > 0
                new_seg = create_segment!(tm, winner_cell)
                grow_new_synapses!(tm, new_seg, num_to_grow,
                                  prev_winning)
            end
        end
    end

    (Set(col_cells), winner_cell)
end

# pseudocode
# fn activate_column!(c)
#   if c has predicting distal segments
#     activate_predicted!(c)
#   else
#     activate_unpredicted!(c)
#   end
# end
function activate_column!(tm::TemporalMemory, col, mpsegments::MPSegments,
                          learn::Bool)
    if !isempty(mpsegments.predicting)
        activated = activate_predicted_column!(tm, col,
            mpsegments.predicting, learn)
        union!(get_active_cells(tm), activated)
        union!(get_winning_cells(tm), activated);
    else
        (activated, winner) = activate_unpredicted_column!(tm, col,
            mpsegments.matching, learn)
        union!(get_active_cells(tm), activated)
        push!(get_winning_cells(tm), winner);
    end
end

function punish_inactive_column!(tm::TemporalMemory, col, col_matching_segs,
                                 prev_active_cells)
    if tm.params.inactive_matching_seg_dec > 0.
        for seg_id in col_matching_segs
            adapt_segment!(tm, seg_id, prev_active_cells,
                           -tm.params.inactive_matching_seg_dec, 0.)
        end
    end
end

# use `params.perm_epsilon` to check if a permanence `p` exceeds
# threshold `thresh`
function perm_exceeds_thresh(tm::TemporalMemory, p::Permanence, thresh)
    p > (thresh - tm.params.perm_epsilon)
end
function perm_exceeds_conn_thresh(tm::TemporalMemory, p::Permanence)
    p > (tm.params.conn_perm_thresh - tm.params.perm_epsilon)
end

function compute_synaptic_activity!(tm::TemporalMemory)
    debug(logger, "computing synaptic activity")

    # `active_cells` is the set of cells active in response to the most
    # recent set of input columns. This pattern of activity determines the
    # pre- and post-synaptic activity levels of all the segments in the
    # network.
    active_cells = get_active_cells(tm)
    num_segs = length(tm.net.segments)
    seg_activities = Dict(segid => SegActivity()
                          for segid in keys(tm.net.segments))

    for cid in active_cells
        for synid in tm.net.cells[cid].target_syns
            segid = get_synapse_segid(tm.net, synid)
            seg_activities[segid].presyn += 1
            syn_perm = get_permanence(tm.net, synid)
            if perm_exceeds_conn_thresh(tm, syn_perm)
                seg_activities[segid].postsyn += 1
            end
        end
    end

    tm.psegments = Set(
        i
        for i in 1:num_segs
        if seg_activities[i].postsyn >= tm.params.seg_predict_thresh
    )

    tm.msegments = Set(
        i
        for i in 1:num_segs
        if seg_activities[i].presyn >= tm.params.seg_match_thresh
    )

    tm.seg_activities = seg_activities
end

# TODO: when ready, learn=true
function compute!(tm::TemporalMemory, active_columns, learn=false)
    debug(logger, "computing w/ cols = $(active_columns) & learn=$(learn))")
    tm.prev_on_cells = Nullable(tm.on_cells)
    tm.on_cells = OnCells()

    prev_on = get_prev_on(tm)
    debug(logger, "Aprev = $(prev_on.active)")
    debug(logger, "Wprev = $(prev_on.winning)")

    for c in 1:num_columns(tm)
        mpsegments = get_column_mp_segs(tm, c)
        if c in active_columns
            activate_column!(tm, c, mpsegments, learn)
        else
            if learn
                punish_inactive_column!(tm, c, mpsegments.matching,
                                        prev_on.active)
            end
        end
    end

    compute_synaptic_activity!(tm)
    if learn
        for seg_id in tm.psegments
            tm.seg_last_predicted[seg_id] = tm.step_num
        end
        tm.step_num += 1
    end
end

function reset!(tm::TemporalMemory)
    tm.on_cells = OnCells()
    tm.psegments = Set()
    tm.msegments = Set()
end

function get_segment_data(tm::TemporalMemory)::SegmentsData
    get_segment_data(tm.net, tm.params.conn_perm_thresh)
end

end

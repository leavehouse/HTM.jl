module SP

import StatsBase

const Permanence = Float64
const SynID = Int
const Index = Tuple{Vararg{Int}}

struct Params
    input_dims::Array{Int, 1}
    column_dims::Array{Int, 1}
    pot_radius::Int
    pot_pct::Float64
    global_inhib::Bool
    inhib_area_density::Float64
    stimulus_thresh::Int
    inactive_syn_perm_dec::Float64
    active_syn_perm_inc::Float64
    syn_perm_conn_thresh::Float64
    duty_cycle_period::Int
    boost_strength::Float64
    Params(input_dims, column_dims, pot_radius, pot_pct, global_inhib,
        inhib_area_density, stimulus_thresh, inactive_syn_perm_dec,
        active_syn_perm_inc, syn_perm_conn_thresh, duty_cycle_period,
        boost_strength) = begin
        D = length(input_dims)
        if length(column_dims) != D
            error("inputs and columns have different numbers of dimensions")
        end
        if D == 0
            error("dimension arrays cannot be empty")
        end
        if prod(input_dims) <= 0
            error("input_dims = $input_dims must have positive product")
        end
        if prod(column_dims) <= 0
            error("column_dims = $column_dims must have positive product")
        end
        if pot_pct <= 0 || pot_pct > 1
            error("pot_pct is $pot_pct, must be > 0 && <= 1")
        end

        # TODO: in nupic, this assert checks that inhib_area_density <= 0.5. Why?
        if !(inhib_area_density > 0 && inhib_area_density <= 1)
            error("inhib_area_density is $inhib_area_density, must be > 0 && <= 1")
        end
        if boost_strength < 0.
            error("boost_strength is $boost_strength, must be nonnegative")
        end

        new(input_dims, column_dims, pot_radius, pot_pct, global_inhib,
            inhib_area_density, stimulus_thresh, inactive_syn_perm_dec,
            active_syn_perm_inc, syn_perm_conn_thresh, duty_cycle_period,
            boost_strength)
    end
end

Params(;
    input_dims = [32],
    column_dims = [64],
    pot_radius = 16,
    pot_pct = 0.5,
    global_inhib = true,
    inhib_area_density = 0.1,
    stimulus_thresh = 0,
    inactive_syn_perm_dec = 0.008,
    active_syn_perm_inc = 0.05,
    syn_perm_conn_thresh = 0.1,
    duty_cycle_period = 1000,
    boost_strength = 0.) = Params(
        input_dims, column_dims, pot_radius, pot_pct, global_inhib,
        inhib_area_density, stimulus_thresh, inactive_syn_perm_dec,
        active_syn_perm_inc, syn_perm_conn_thresh, duty_cycle_period,
        boost_strength)

num_dims(params::Params) = length(params.column_dims)
num_inputs(params::Params) = prod(params.input_dims)
num_columns(params::Params) = prod(params.column_dims)

function dims_indices(dims::Array{Int, 1})
    Iterators.product([1:d for d in dims])
end

column_indices(params::Params) = dims_indices(params.column_dims)

function column_input_center(params::Params, col::Index)::Index
    if length(col) != num_dims(params)
        error("column index doesn't match number of dimensions of SP")
    end
    tuple([map_center_1d(I, C, k) for (I, C, k) in
           zip(params.input_dims, params.column_dims, col)]...)
end

# Given a 1-D array of `I` inputs and a 1-D array of `C` columns, determines
# the center of input for column `k`.
function map_center_1d(I::Int, C::Int, k::Int)::Int
    # Start with the closed interval [0, I] of the real line. This interval can
    # be partitioned into I equal-width *input bins*, where the j-th bin is the
    # interval [j-1, j] for j = 1, ..., I.
    #
    # Now we partition the interval [0, I] into `C` equal segments. Each
    # resultant segment, which we call a *column segment*, has a width I/C.
    #
    # The center of input for column `k` is the index of the input bin that
    # contains the midpoint of the `k`-th column segment.
    #
    # To calculate this we first calculate the midpoint of the k-th column
    # segment [(k-1)*(I/C), k*(I/C)]:
    #
    #  ((k-1) + k)*(I/C)/2 = (k - 0.5)*(I/C)
    #
    # To finish calculating the input bin, simply take the ceiling of this
    # midpoint
    if k < 1 || k > C
        error("column is $k, must be in {1, ..., C}")
    end
    ceil((k - 0.5) * I / C)
end

mutable struct Neighborhood
    center::Index
    radius::Int
    dims::Array{Int, 1}
    indices::Iterators.Prod2
end

Neighborhood(center::Index, radius::Int, dims::Array{Int, 1}) = begin
    dim_bounds = [max(center[i] - radius, 1):min(center[i] + radius, dims[i])
                  for i in 1:length(dims)]
    indices = Iterators.product(dim_bounds...)
    Neighborhood(center, radius, dims, indices)
end

Base.start(n::Neighborhood) = Base.start(n.indices)
Base.next(n::Neighborhood, state) = Base.next(n.indices, state)
Base.done(n::Neighborhood, state) = Base.done(n.indices, state)

struct PotentialSynapse
    presyn_cell::Index
    permanence::Permanence
end

# TODO: currently no boosting, no duty cycle stuff
mutable struct SpatialPooler
    params::Params
    num_dims::Int
    num_inputs::Int
    num_columns::Int

    # Each column has a set of cells from which it is (potentially) possible to
    # receive input, the *potential pool*. Each of these connections has an
    # associated *permanence* value, and the synapse is only considered to be
    # *connected* if the permanence is above a threshold.
    synapses::Dict{SynID, PotentialSynapse}
    potential_pools::Dict{Index, Set{SynID}}
    inhib_radius::Nullable{Int}
end

SpatialPooler(params::Params) = begin
    (syns, ppools) = init_ppools(params)
    sp = SpatialPooler(params, num_dims(params), num_inputs(params),
        num_columns(params), syns, ppools, Nullable())

    update_inhib_radius!(sp)
    sp
end

# TODO: duplication of code in network.jl
function next_id(d::Dict{Int,T})::Int where {T}
    if !isempty(d)
        # get max key in the dict, add 1 to get the next SegID
        Base.Checked.checked_add(maximum(keys(d)), 1)
    else
        1
    end
end

function get_synapse(sp::SpatialPooler, synid::SynID)
    if !haskey(sp.synapses, synid)
        error("PotentialSynapse $(synid) does not exist")
    end
    sp.synapses[synid]
end

# Returns the set of presynaptic cells for column `col`
function init_ppool_prepool(params::Params, col::Index)::Set{Index}
    input_center = column_input_center(params, col)
    nhood = [n for n in Neighborhood(input_center, params.pot_radius,
                                     params.input_dims)]
    pool_size = round(length(nhood) * params.pot_radius)
    StatsBase.sample(nhood, pool_size, replace=false)
end

function init_ppool_permanence(params::Params, conn_frac::Permanence)
    if random.random() <= conn_frac
        (params.syn_perm_conn_thresh +
         (1 - params.syn_perm_conn_thresh) * random.random())
    else
        params.syn_perm_conn_thresh * random.random()
    end
end

function init_ppool(params::Params, col::Index)::Set{PotentialSynapse}
    presyn_cells = init_ppool_prepool(params, col)
    init_perm() = init_ppool_permanence(params)
    Set(PotentialSynapse(presyn, init_perm()) for presyn in
         presyn_cells)
end

function init_ppools(params::Params)
    syns = Dict{SynID, PotentialSynapse}()
    ppools = Dict{Index, Set{SynID}}()
    for col in column_indices(params)
        pool = init_ppool(params, col)
        col_synids = Set{SynID}()
        for new_pot_syn in pool
            new_synid = next_id(syns)
            syns[new_synid] = new_pot_syn
            push!(col_synids, new_synid)
        end
        ppools[col] = col_synids
    end
    (syns, ppools)
end

# average the receptive field diameter over the dimensions (receptive field =
# connected pool)
function avg_receptive_field_diam(sp::SpatialPooler, col::Index)::Float64
    D = sp.num_dims
    diam = 0
    conn_pool = (get_synapse(sp, synid).presyn_cell for synid in
                 connected_pool(sp, col))
    for i in 1:D
        # find the diameter of the RF along the i-th dimension
        field_begin = sp.params.input_dims[i]
        field_end = 1
        for input in conn_pool
            if input[i] < field_begin
                field_begin = input[i]
            end
            if input[i] > field_end
                field_end = input[i]
            end
        end
        diam += field_end - field_begin + 1
    end
    diam/D
end

# calculate the columns per input along each dimension, average over dimensions
function avg_columns_per_input(sp::SpatialPooler)
    D = sp.num_dims
    columns_per_input = 0
    for i in 1:D
        columns_per_input += sp.params.column_dims[i] / sp.params.input_dims[i]
    end
    columns_per_input / D
end

function update_inhib_radius!(sp::SpatialPooler)
    sp.inhib_radius =  if sp.params.global_inhib
        Nullable{Int}()
    else
        avg_rf_diam = 0
        for col in column_indices(params)
            avg_rf_diam += avg_receptive_field_diam(sp, col)
        end
        avg_rf_diam /= num_columns(params)
        diameter = avg_rf_diam * avg_columns_per_input(sp)
        # TODO: why the minus 1?
        radius = (diameter - 1)/2
        Nullable(max(round(radius), 1))
    end
end

function connected_pool(sp::SpatialPooler, col::Index, perm_thresh::Permanence)
    Set(synid for synid in sp.potential_pools[col]
        if get_synapse(synid).permanence >= perm_thresh)
end

connected_pool(sp::SpatialPooler, col::Index) = connected_pool(
    sp, col, sp.params.syn_perm_conn_thresh)

function get_active_global(sp::SpatialPooler,
                           overlaps::Dict{Index, Int})::Set{Index}
    function potentially_active(col::Index, pot_active::Array{Index, 1},
                                num_active_target::Int)
        (overlaps[col] >= sp.params.stimulus_thresh &&
         (length(pot_active) < num_active_target ||
          overlaps[col] > overlaps[pot_active[num_active_target]]))
    end

    # insert column into pot_active in decreasing order of overlap score
    function add_to_pot_active(col::Index, pot_active::Array{Index, 1})
        # find the first i for which col's overlap meets or exceeds
        # pot_active[i]
        for i in 1:length(pot_active)
            if overlaps[col] >= overlaps[pot_active[i]]
                insert!(pot_active, i, col)
                return
            end
        end
        push!(pot_active, col);
    end

    # TODO: look at tiebreaking. i think ties go to most recently added
    # TODO: why is num_active_target calculated this way
    num_active_target = round(sp.params.inhib_area_density * sp.num_columns)
    pot_active = []
    for col in column_indices(sp)
        if potentially_active(col, pot_active, num_active_target)
            add_to_pot_active(col, pot_active)
        end
    end
    num_active = min(num_active_target, length(pot_active))
    pot_active[1:num_active]
end

function get_active_local(sp::SpatialPooler,
                          overlaps::Dict{Index, Int})::Set{Index}

    function calc_inhib_area_stats(col::Index)
        nhood = Neighborhood(col, sp.inhib_radius,
                             sp.params.column_dims)
        num_neighbors = 0
        num_higher_overlap = 0
        for neighbor in nhood
            if neighbor != col
                num_neighbors += 1
                diff = overlaps[neighbor] - overlaps[col]
                if diff > 0 || (diff == 0 && neighbor in active)
                    num_higher_overlap += 1
                end
            end
        end
        (num_neighbors, num_higher_overlap)
    end

    # TODO: tiebreaking. ties go to first added.
    active = Set()
    for col in column_indices(sp)
        if overlap[col] >= sp.params.stimulus_thresh
            (num_neighbors, num_higher_overlap) = calc_inhib_area_stats(col)
            num_active = round(sp.params.inhib_area_density * (num_neighbors + 1))
            if num_higher_overlap < num_active
                push!(active, col)
            end
        end
    end
    active
end

# Currently this just keeps the permanence bounded between 0 and 1, but
# will eventually (probably) do other things like trimming
function update_column_perms(sp::SpatialPooler,
                             new_perms::Dict{SynID, Permanence}, col::Index)
    # TODO: raisePerm business
    for (synid, new_perm) in keys(new_perms)
        # TODO: for permanences below trim threshold, set perm to zero
        sp.synapses[synid].permanence = clamp(new_perm, 0., 1.)
    end
end

function adapt_synapses!(sp::SpatialPooler, input_vec::BitArray, active_columns)
    perm_deltas = [(bit == 1 ? sp.params.active_syn_perm_inc
                    : -sp.params_inactive_syn_perm_dec) for bit in input_vec]
    for col in active_columns
        ppool = sp.potential_pools[col]
        col_perms = Dict()
        # for all potential synapses
        for synid in ppool
            syn = get_synapse(sp, synid)
            col_perms[synid] = syn.permanence + perm_deltas[syn.presyn_cell]
        end
        update_column_perms!(sp, col_perms, col)
    end
end

# apply inhibition to get winning cells
function inhibit_columns(sp::SpatialPooler)
    if isnull(sp.inhib_radius)
        # global inhibition
        get_active_global(sp, overlaps)
    else
        get_active_local(sp, overlaps)
    end
end

# returns the set of active columns
function compute!(sp::SpatialPooler, input_vec::BitArray,
                  learn::Bool=false)::Set{Index}
    if length(input_vec) != sp.num_inputs
        error("Input vector length does not match SP's number of inputs")
    end

    overlaps = Dict(col => length(z & connected_pool(sp, col))
                   for col in column_indices(sp.params))

    # TODO: boosted overlap

    active_cols = inhibit_columns(sp)

    if learn
        # Update permanences via Hebbian-like learning
        for col in column_indices(sp)
            for synid in sp.potential_pools[col]
                syn = get_synapse(sp, synid)
                if syn.presyn_cell in active_cols
                    syn.permanence = min(1, syn.permanence +
                                         sp.params.active_syn_perm_inc)
                else
                    syn.permanence = max(0, syn.permanence -
                                         sp.params.inactive_syn_perm_dec)
                end
            end
        end
        update_inhib_radius!(sp)
    end
    active
end

end

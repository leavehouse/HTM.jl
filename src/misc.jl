# Using `maximum` on dicts is wonky in Julia 0.6, so
# we use this workaround until it's fixed
function dictmax{K,V}(d::Dict{K,V})
    @assert length(d) > 0
    maxkey = nothing
    maxval = nothing
    for (k,v) in d
        if maxkey == nothing || d[k] > maxval
            maxkey = k
            maxval = d[k]
        end
    end
    Pair(maxkey,maxval)
end

function dictmin{K,V}(d::Dict{K,V})
    @assert length(d) > 0
    minkey = nothing
    minval = nothing
    for (k,v) in d
        if minkey == nothing || d[k] < minval
            minkey = k
            minval = d[k]
        end
    end
    Pair(minkey,minval)
end

const CellID = Int
const SegID = Int
const SynID = Int
const CellIDs = Set{CellID}
const SegIDs = Set{SegID}
const SynIDs = Set{SynID}
const Permanence = Float64

type PotentialSynapse
    presyn_cell::CellID
    segment::SegID
    permanence::Permanence
end

type Segment
    postsyn_cell::CellID # the segment is attached to this cell
    synapses::SynIDs
end
Segment(postsyn_cid::CellID) = Segment(postsyn_cid, Set())

type Cell
    segments::SegIDs
    target_syns::SynIDs
end
Cell() = Cell(Set(), Set())

"The segments and synapses for the cells in a TM region."
type RegionNetwork
    num_cells::Int
    segments::Dict{SegID, Segment}
    synapses::Dict{SynID, PotentialSynapse}
    cells::Array{Cell, 1}
end
RegionNetwork(num_cells::Int) = RegionNetwork(num_cells, Dict(), Dict(),
                                              [Cell() for _ in 1:num_cells])

const SegmentSynapses = Dict{CellID, Permanence}
type SegmentData
    synapses::SegmentSynapses
    connected::CellIDs
end
const SegmentsData = Dict{SegID, SegmentData}

# SegIDs & SynIDs don't get re-used. instead we just keep generating
# bigger ones. TODO: space savings by recycling numbers? could use a
# smaller datatype to store the ID.
function next_id(d::Dict{Int,T})::Int where {T}
    if !isempty(d)
        # get max key in the dict, add 1 to get the next SegID
        Base.Checked.checked_add(maximum(keys(d)), 1)
    else
        1
    end
end

function next_segid(net::RegionNetwork)::SegID
    next_id(net.segments)
end

function next_synid(net::RegionNetwork)::SynID
    next_id(net.synapses)
end

function get_cell_segments(net::RegionNetwork, cid::CellID)::SegIDs
    net.cells[cid].segments
end

function get_num_segments(net::RegionNetwork, cid::CellID)::Int
    length(get_cell_segments(net, cid))
end

function get_num_segments(net::RegionNetwork)::Int
    length(net.segments)
end

"""
    create_segment!(net::RegionNetwork, cid)

Create a segment on cell `cid`. Return the `SegID` of the created segment.
"""
function create_segment!(net::RegionNetwork, cid::CellID)::SegID
    new_segid = next_segid(net)
    net.segments[new_segid] = Segment(cid)
    push!(net.cells[cid].segments, new_segid)
    new_segid
end

function get_segment(net::RegionNetwork, segid::SegID)::Segment
    if !haskey(net.segments, segid)
        error("Segment $(segid) does not exist")
    end
    return net.segments[segid]
end

function get_segment_synapses(net::RegionNetwork, segid::SegID)::SynIDs
    get_segment(net, segid).synapses
end

function get_segment_num_synapses(net::RegionNetwork, segid::SegID)
    length(get_segment_synapses(net, segid))
end

"""
    destroy_segment!(net::RegionNetwork, segid)

Destroy the segment `segid` and all of its synapses.
"""
function destroy_segment!(net::RegionNetwork, segid::SegID)
    for synid in get_segment_synapses(net, segid)
        destroy_synapse!(net, synid)
    end

    postsyn_cell = get_segment(net, segid).postsyn_cell
    delete!(net.cells[postsyn_cell].segments, segid)
    delete!(net.segments, segid);
    # TODO: maintin a list of free-to-use SegIDs (aka recycle the SegIDs of deleted segments), or just keep incrementing?
end

"""
    create_synapse!(net::RegionNetwork, segid, presyn_cid, init_perm)

Create a synapse on segment `segid` to presynaptic cell `presyn_cid` with
initial permanence `init_perm`. Return the `SynID` of the new synapse.
"""
function create_synapse!(net::RegionNetwork, segid::SegID, presyn_cid::CellID, init_perm::Permanence)::SynID
    new_synid = next_synid(net)
    new_synapse = PotentialSynapse(presyn_cid, segid, init_perm)
    net.synapses[new_synid] = new_synapse
    push!(get_segment_synapses(net, segid), new_synid)

    # update the presyn cell's set of target synapses
    push!(net.cells[presyn_cid].target_syns, new_synid)
    new_synid
end

function get_synapse(net::RegionNetwork, synid::SynID)::PotentialSynapse
    if !haskey(net.synapses, synid)
        error("PotentialSynapse $(synid) does not exist")
    end
    return net.synapses[synid]
end

# TODO: remove?
function get_synapse_segid(net::RegionNetwork, synid::SynID)::SegID
    get_synapse(net, synid).segment
end

function get_synapse_segment(net::RegionNetwork, synid::SynID)::Segment
    segid = get_synapse_segid(net, synid)
    get_segment(net, segid)
end

"""
    destroy_synapse!(net::RegionNetwork, synid)

Destroy the synapse `synid`.
"""
function destroy_synapse!(net::RegionNetwork, synid::SynID)
    # Remove the synapse from the presyn cell's `target_syns`
    presyn_cell = get_synapse(net, synid).presyn_cell
    presyn_target_syns = net.cells[presyn_cell].target_syns
    delete!(presyn_target_syns, synid)

    delete!(get_synapse_segment(net, synid).synapses, synid)
    delete!(net.synapses, synid)
end

function get_permanence(net::RegionNetwork, synid::SynID)
    get_synapse(net, synid).permanence
end

function set_permanence!(net::RegionNetwork, synid::SynID, perm::Permanence)
    synapse = get_synapse(net, synid)
    synapse.permanence = perm
end

function get_segment_data(net::RegionNetwork, conn_perm_thresh)::SegmentsData
    threshold = conn_perm_thresh
    seg_data = SegmentsData()
    for segid in keys(net.segments)
        synapses = SegmentSynapses()
        connected = Set()

        for synid in get_segment_synapses(net, segid)
            syn = get_synapse(net, synid)
            synapses[syn.presyn_cell] = syn.permanence

            if syn.permanence > threshold
                push!(connected, syn.presyn_cell)
            end
        end
        seg_data[segid] = SegmentData(synapses, connected)
    end
    seg_data
end

"""
    cell_with_fewest_segs(net::RegionNetwork, cells)

Return the cell with the fewest number of segments out of the iterable `cells`.
"""
function cell_with_fewest_segs(net::RegionNetwork, cells)
    if isempty(cells)
        return nothing
    end

    fewest = []
    min_num_segs = nothing
    
    for cid in cells
        num_segs = get_num_segments(net, cid)
        if min_num_segs == nothing || num_segs < min_num_segs
            fewest = []
            min_num_segs = num_segs
        end
        
        if num_segs == min_num_segs
            push!(fewest, cid)
        end
    end
    
    # randomly return one of the cells with the fewest
    # number of segments
    fewest[rand(1:length(fewest))]
end

@testset "testCompute1" begin
    params = SP.Params(
        input_dims=[9],
        column_dims=[5],
        pot_radius=3,
        pot_pct=0.5,
        global_inhib=false,
        inhib_area_density=0.2,
        #numActiveColumnsPerInhArea=3,
        stimulus_thresh=1,
        inactive_syn_perm_dec=0.1,
        active_syn_perm_inc=0.1,
        syn_perm_conn_thresh=0.10,
        duty_cycle_period=10,
        boost_strength=10.0)
    sp = SP.SpatialPooler(params)

    # TODO: figure out how to replace these two lines
    sp._potentialPools = BinaryCorticalColumns(numpy.ones([sp._numColumns,
                                                           sp._numInputs]))
    sp._inhibitColumns = Mock(return_value = numpy.array(range(5)))

    input_vec = BitArray([1, 0, 1, 0, 1, 0, 0, 1, 1])
    for _ in 1:20
        compute!(sp, input_vec, true)
    end

    for col in 1:sp.num_columns
        col_perms = [get_synapse(sp, synid).permanence for synid in sp.potential_pools[col]]
        for j in 1:sp.num_inputs
            @test input_vec[j] == col_perms[j]
        end
    end
end

using HTM
const TM = HTM.TM
const SP = HTM.SP
using Base.Test

@testset "Temporal Memory" begin
    tests = ["SpatialPooler", "network", "TemporalMemory"]
    for test in tests
        @testset "$test" begin
            include("$test.jl")
        end
    end
end

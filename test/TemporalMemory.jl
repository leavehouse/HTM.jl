# written to be included from runtests.jl

function create_test_region()::TM.TemporalMemory
    params = TM.Params(
      col_dims = [32],
      cells_per_col = 4,
      seg_predict_thresh = 3,
      seg_match_thresh = 2,
      conn_perm_thresh = 0.5,
      perm_inc = 0.1,
      perm_dec = 0.1,
      max_new_syn_count = 3,
      new_syn_init_perm = 0.21)
    TM.TemporalMemory(params)
end

type CreatedSegment
    segid::TM.SegID
    synids::Array{TM.SynID, 1}
end
const Synapses = Array{Pair{TM.CellID, TM.Permanence}, 1}
# `segments` is an iterable of `(CellID, Synapses)` tuples
function create_segments!(tm::TM.TemporalMemory,
                          segments)::Array{CreatedSegment, 1}
    created = CreatedSegment[]
    for (cell::TM.CellID, syns::Synapses) in segments
        segid = TM.create_segment!(tm, cell)
        seg_syns = [TM.create_synapse!(tm.net, segid, presyn, perm)
                    for (presyn, perm) in syns]
        push!(created, CreatedSegment(segid, seg_syns))
    end
    created
end

@testset "activate_unpredicted_col" begin
    tm = create_test_region()

    active_cols = Set([1])
    active_cells = Set([1, 2, 3, 4])
    TM.compute!(tm, active_cols, true)
    @test active_cells == TM.get_active_cells(tm)
end

@testset "activate_predicted_col" begin
    tm = create_test_region()
    M = tm.params.cells_per_col

    # The idea is that
    #   1) we are going to set up one synapse from each cell in col 1
    #      to the cell pred := (M+1), the "first" cell in col 2.
    #   1a) the initial permanence for each synapse will be such that
    #       the synapse is initially connected
    #   2) we will do two compute steps in succession, the first with
    #      column 1 activated, the second with column 2
    #   3) we test
    #     a) whether `pred` is the only predicted cell after compute 1
    #     b) whether `pred` is the only active cell after compute 2
    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    active_cols = Set([2])
    pred = M + 1
    pred_cells = Set([pred])

    segment = [(pred, [c => 0.5 for c in prev_active_cells])]
    create_segments!(tm, segment)

    TM.compute!(tm, prev_active_cols, true)
    @test pred_cells == TM.get_predicted_cells(tm)
    TM.compute!(tm, active_cols, true)
    @test pred_cells == TM.get_active_cells(tm)
end

@testset "no_active_columns" begin
    tm = create_test_region()
    M = tm.params.cells_per_col

    # The idea is that
    #   1) we are going to set up one synapse from each cell in col 1
    #      to the cell pred := (M+1), the "first" cell in col 2.
    #     a) the initial permanence for each synapse will be such that
    #        the synapse is initially connected
    #   2) we will do two compute steps in succession, the first with
    #      column 1 activated, the second with no active columns
    #   3) the expected result of a compute! step with no active
    #      columns is no on-cells and no predicted cells, so we test
    #     a) whether compute 1 results in at least one of each category
    #        in {active, winning, predicted}
    #     b) whether compute 2 results in exactly zero active/winning/
    #        predicted cells
    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    pred = M + 1

    segment = [(pred, [c => 0.5 for c in prev_active_cells])]
    create_segments!(tm, segment)

    TM.compute!(tm, prev_active_cols, true)
    @test length(TM.get_active_cells(tm)) > 0
    @test length(TM.get_winning_cells(tm)) > 0
    @test length(TM.get_predicted_cells(tm)) > 0

    TM.compute!(tm, Set(), true)
    @test length(TM.get_active_cells(tm)) == 0
    @test length(TM.get_winning_cells(tm)) == 0
    @test length(TM.get_predicted_cells(tm)) == 0
end

@testset "predicted_active_cells_win" begin
    tm = create_test_region()
    M = tm.params.cells_per_col
    seg_predict_thresh = tm.params.seg_predict_thresh

    prev_active_cols = Set([1])
    active_cols = Set([2])
    prev_active_cells = [1:M;]
    pred_cells = Set([M+1, M+3])

    seg_syns = [prev_active_cells[i] => 0.5 for i in 1:seg_predict_thresh]
    create_segments!(tm, [(pred, seg_syns) for pred in pred_cells])

    # TODO: why learn=false?
    TM.compute!(tm, prev_active_cols, false)
    TM.compute!(tm, active_cols, false)
    @test pred_cells == TM.get_winning_cells(tm)
end

@testset "reinforce_syns_of_postsyn_active_cell" begin
    tm = create_test_region()
    tm.params.max_new_syn_count = 4
    tm.params.perm_dec = 0.08
    tm.params.inactive_matching_seg_dec = 0.02
    M = tm.params.cells_per_col
    seg_predict_thresh = tm.params.seg_predict_thresh

    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    active_cols = Set([2])
    pred = M + 1

    segid = TM.create_segment!(tm, pred)
    reinf_synids = []
    for i in 1:seg_predict_thresh
        synid = TM.create_synapse!(tm.net, segid, prev_active_cells[i],
                                   0.5)
        push!(reinf_synids, synid)
    end
    inactive_synid = TM.create_synapse!(tm.net, segid, tm.net.num_cells,
                                        0.5)

    TM.compute!(tm, prev_active_cols, true)
    TM.compute!(tm, active_cols, true)

    # TODO: test that this is a close enough replacement
    for synid in reinf_synids
        @test 0.6 ≈ TM.get_permanence(tm.net, synid)
    end
    @test 0.42 ≈ TM.get_permanence(tm.net, inactive_synid)
end

@testset "adjust_msegs_and_non_msegs_in_unpredicted_col" begin
    tm = create_test_region()
    tm.params.perm_dec = 0.08
    M = tm.params.cells_per_col

    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    active_cols = Set([2])
    active_cells = Set([(M+1):(2*M);])

    # Create segments on cells `active_cells[1:2]`
    segments = [
        (M+1, [c => 0.3 for c in [prev_active_cells[1:3]; tm.net.num_cells]]),
        (M+2, [c => 0.3 for c in [prev_active_cells[1:2]; tm.net.num_cells]])]
    created = create_segments!(tm, segments)

    TM.compute!(tm, prev_active_cols, true)
    TM.compute!(tm, active_cols, true)

    @test 0.4 ≈ TM.get_permanence(tm.net, created[1].synids[1])
    @test 0.4 ≈ TM.get_permanence(tm.net, created[1].synids[2])
    @test 0.4 ≈ TM.get_permanence(tm.net, created[1].synids[3])
    @test 0.22 ≈ TM.get_permanence(tm.net, created[1].synids[4])
    @test 0.3 ≈ TM.get_permanence(tm.net, created[2].synids[1])
    @test 0.3 ≈ TM.get_permanence(tm.net, created[2].synids[2])
    @test 0.3 ≈ TM.get_permanence(tm.net, created[2].synids[3])
end

@testset "no_change_to_msegs_in_predicted_active_col" begin
    tm = create_test_region()
    M = tm.params.cells_per_col

    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    active_cols = Set([2])
    exp_active_cell = M+1
    other_cell = exp_active_cell + 1

    segments = [
        (exp_active_cell, [prev_active_cells[i] => 0.5 for i in 1:4]),
        (exp_active_cell, [prev_active_cells[i] => 0.3 for i in 1:2]),
        (other_cell, [prev_active_cells[i] => 0.3 for i in 1:2])
    ]
    created = create_segments!(tm, segments)

    TM.compute!(tm, prev_active_cols, true)
    @test Set([exp_active_cell]) == TM.get_predicted_cells(tm)
    TM.compute!(tm, active_cols, true)

    @test 0.3 ≈ TM.get_permanence(tm.net, created[2].synids[1])
    @test 0.3 ≈ TM.get_permanence(tm.net, created[2].synids[2])
    @test 0.3 ≈ TM.get_permanence(tm.net, created[3].synids[1])
    @test 0.3 ≈ TM.get_permanence(tm.net, created[3].synids[2])
end

@testset "no_new_segment_if_not_enough_winner_cells" begin
    tm = create_test_region()
    TM.compute!(tm, Set(), true)
    TM.compute!(tm, Set([1]), true)
    @test TM.get_num_segments(tm) == 0
end

@testset "new_seg_grows_synapses_to_prev_winner_cells" begin
    tm = create_test_region()
    tm.params.max_new_syn_count = 2

    prev_active_cols = Set(1:3)
    active_cols = Set([4])

    TM.compute!(tm, prev_active_cols, true)
    prev_winner_cells = TM.get_winning_cells(tm)
    @test length(prev_winner_cells) == 3

    TM.compute!(tm, active_cols, true)
    winner_cells = TM.get_winning_cells(tm)
    @test length(winner_cells) == 1
    winner_cell = [c for c in winner_cells][1]
    segids = TM.get_cell_segments(tm.net, winner_cell)
    @test length(segids) == 1
    synids = TM.get_segment_synapses(tm.net, [s for s in segids][1])
    @test length(synids) == 2

    for synid in synids
        @test TM.get_permanence(tm.net, synid) ≈ 0.21
        @test TM.get_synapse(tm.net, synid).presyn_cell in prev_winner_cells
    end
end

# TODO: this is almost exactly the same test as
# `grown_segment_has_preseg_in_prev_winner_cells`. The max_new_syn_count
# being 4 instead of two causes us to be able to grow synapses to all
# previous winner cells. Not sure why this specific thing is desirable to test
# for, look to combine with aforementioned test.
@testset "new_seg_grows_synapses_to_all_prev_winner_cells" begin
    tm = create_test_region()
    tm.params.max_new_syn_count = 4

    prev_active_cols = Set(1:3)
    active_cols = Set([TM.num_columns(tm)])

    TM.compute!(tm, prev_active_cols, true)
    prev_winner_cells = TM.get_winning_cells(tm)
    @test length(prev_winner_cells) == 3

    TM.compute!(tm, active_cols, true)
    winner_cells = TM.get_winning_cells(tm)
    @test length(winner_cells) == 1
    winner_cell = [c for c in winner_cells][1]
    segids = TM.get_cell_segments(tm.net, winner_cell)
    @test length(segids) == 1
    synids = TM.get_segment_synapses(tm.net, [s for s in segids][1])
    @test length(synids) == 3

    presyn_cells = []
    for synid in synids
      @test TM.get_permanence(tm.net, synid) ≈ 0.21
      presyn_cell = TM.get_synapse(tm.net, synid).presyn_cell
      push!(presyn_cells, presyn_cell)
    end
    @test prev_winner_cells == Set(presyn_cells)
end

    tm = create_test_region()
    tm.params.cells_per_col = 1
    tm.params.seg_match_thresh = 1

@testset "mseg_grows_synapses_to_prev_winner_cells" begin
    tm = create_test_region()
    tm.params.cells_per_col = 1
    tm.params.seg_match_thresh = 1

    prev_active_cols = Set(1:4)
    prev_winner_cells = [1:4;]
    active_cols = Set([5])
    active_cell = [c for c in active_cols][1]

    segid = TM.create_segment!(tm, active_cell)
    TM.create_synapse!(tm.net, segid, 1, 0.5)

    TM.compute!(tm, prev_active_cols, true)
    @test Set(prev_winner_cells) == TM.get_winning_cells(tm)
    TM.compute!(tm, active_cols, true)

    synids = TM.get_segment_synapses(tm.net, segid)
    @test length(synids) == 3

    for synid in synids
        presyn_cell = TM.get_synapse(tm.net, synid).presyn_cell
        if presyn_cell != 1
            @test TM.get_permanence(tm.net, synid) ≈ 0.21
            @test presyn_cell in prev_winner_cells[2:4]
        end
    end
end

# TODO: combine with above?
@testset "mseg_grows_synapses_to_all_prev_winner_cells" begin
    tm = create_test_region()
    tm.params.cells_per_col = 1
    tm.params.seg_match_thresh = 1

    prev_active_cols = Set(1:2)
    prev_winner_cells = [1, 2]
    active_cols = Set([5])
    active_cell = [c for c in active_cols][1]

    segid = TM.create_segment!(tm, active_cell)
    TM.create_synapse!(tm.net, segid, 1, 0.5)

    TM.compute!(tm, prev_active_cols, true)
    @test Set(prev_winner_cells) == TM.get_winning_cells(tm)
    TM.compute!(tm, active_cols, true)

    synids = TM.get_segment_synapses(tm.net, segid)
    @test length(synids) == 2

    for synid in synids
        presyn_cell = TM.get_synapse(tm.net, synid).presyn_cell
        if presyn_cell != 1
            @test TM.get_permanence(tm.net, synid) ≈ 0.21
            @test presyn_cell == prev_winner_cells[2]
        end
    end
end

@testset "pseg_grow_synapses_according_to_potential_overlap" begin
    tm = create_test_region()
    tm.params.cells_per_col = 1
    tm.params.seg_predict_thresh = 2
    tm.params.seg_match_thresh = 1
    tm.params.max_new_syn_count = 4

    prev_active_cols = Set(1:5)
    prev_winning_cells = [1:5;]
    active_cell = 6
    active_cols = Set([active_cell])

    segment = [(active_cell, [1 => 0.5, 2 => 0.5, 3 => 0.2])]
    created = create_segments!(tm, segment)

    TM.compute!(tm, prev_active_cols, true)
    @test Set(prev_winning_cells) == TM.get_winning_cells(tm)
    TM.compute!(tm, active_cols, true)

    synids = TM.get_segment_synapses(tm.net, created[1].segid)
    presyn_cells = Set(TM.get_synapse(tm.net, synid).presyn_cell
                       for synid in synids)

    @test (presyn_cells == Set([1:3; 4])
           || presyn_cells == Set([1:3; 5]))
end

@testset "destroy_weak_synapse_on_wrong_prediction" begin
    tm = create_test_region()
    tm.params.new_syn_init_perm = 0.2
    tm.params.max_new_syn_count = 4
    tm.params.inactive_matching_seg_dec = 0.02
    M = tm.params.cells_per_col

    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    active_cols = Set([3])
    pred = M + 2

    segment = [(pred, [[p => 0.5 for p in prev_active_cells[1:3]];
                       prev_active_cells[4] => 0.015])]
    created = create_segments!(tm, segment)

    TM.compute!(tm, prev_active_cols, true)
    TM.compute!(tm, active_cols, true)
    @test TM.get_segment_num_synapses(tm.net, created[1].segid) == 3
end


# TODO: the only difference between this and previous test is the weak
# synapse's permanence (0.015 versus 0.009). Why have separate tests?
@testset "destroy_weak_synapse_on_active_reinforce" begin
    tm = create_test_region()
    tm.params.new_syn_init_perm = 0.2
    tm.params.max_new_syn_count = 4
    tm.params.inactive_matching_seg_dec = 0.02
    M = tm.params.cells_per_col

    prev_active_cols = Set([1])
    prev_active_cells = [1:M;]
    active_cols = Set([3])
    pred = M + 2

    segment = [(pred, [[p => 0.5 for p in prev_active_cells[1:3]];
                       prev_active_cells[4] => 0.009])]
    created = create_segments!(tm, segment)

    TM.compute!(tm, prev_active_cols, true)
    TM.compute!(tm, active_cols, true)
    @test TM.get_segment_num_synapses(tm.net, created[1].segid) == 3
end

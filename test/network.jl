# written to be included from runtests.jl
# TODO: test multiple segments, compute_activity, get_segment_data,
num_cells = 6
net = TM.RegionNetwork(num_cells)

function test_num_segments(total, per_cell)
    @test TM.get_num_segments(net) == total
    for i in 1:num_cells
        @test TM.get_num_segments(net, i) == per_cell[i]
    end
end

function test_new_synapse(segid, presyn_cid, new_synid, exp_synid,
                          exp_perm, exp_seg_num_syns)
    @test new_synid == exp_synid
    syn = TM.get_synapse(net, new_synid)
    @test syn.presyn_cell == presyn_cid
    @test syn.segment == segid
    @test syn.permanence == exp_perm
    @test new_synid in TM.get_segment_synapses(net, segid)
    @test TM.get_segment_num_synapses(net, segid) == exp_seg_num_syns
end

test_num_segments(0, [0 for i in 1:num_cells])

@test TM.next_segid(net) == 1
@test TM.next_synid(net) == 1

# create a segment on this CellID
cella = 1
segid = TM.create_segment!(net, cella)
@test segid == 1
test_num_segments(1, [1,0,0,0,0,0])
seg = TM.get_segment(net, segid)
@test seg.postsyn_cell == cella
@test isempty(TM.get_segment_synapses(net, segid))
@test TM.get_segment_num_synapses(net, segid) == 0

@test TM.next_segid(net) == 2
@test TM.next_synid(net) == 1

# create a synapse on `segid`
presyn_cell_a = 2
perm_a = 0.1
synid_a = TM.create_synapse!(net, segid, presyn_cell_a, perm_a)
test_new_synapse(segid, presyn_cell_a, synid_a, 1, perm_a, 1)
@test length(net.synapses) == 1

@test TM.next_segid(net) == 2
@test TM.next_synid(net) == 2

# and another one for good measure
presyn_cell_b = 3
perm_b = 0.2
synid_b = TM.create_synapse!(net, segid, presyn_cell_b, perm_b)
test_new_synapse(segid, presyn_cell_b, synid_b, 2, perm_b, 2)
@test length(net.synapses) == 2

# delete the first synapse we created
TM.destroy_synapse!(net, synid_a)
@test_throws ErrorException TM.get_synapse(net, synid_a)
@test !(synid_a in TM.get_segment_synapses(net, segid))
@test synid_b in TM.get_segment_synapses(net, segid)
@test TM.get_segment_num_synapses(net, segid) == 1
@test length(net.synapses) == 1

# destroy the segment
TM.destroy_segment!(net, segid)
@test_throws ErrorException TM.get_segment(net, segid)
test_num_segments(0, [0,0,0,0,0,0])
@test length(net.synapses) == 0

# cell_with_fewest_segs
net = TM.RegionNetwork(num_cells)
cell_num_segs = [3, 2, 1, 6, 5, 4]
for cid in 1:num_cells
    for _ in 1:cell_num_segs[cid]
        TM.create_segment!(net, cid)
    end
end
@test TM.cell_with_fewest_segs(net, 1:num_cells) == 3
@test TM.cell_with_fewest_segs(net, []) == nothing

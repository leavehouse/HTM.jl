using Documenter, TemporalMemory

makedocs(
    modules = [TemporalMemory],
    format = :html,
    sitename = "TemporalMemory.jl",
    pages = [
        "Overview" => "index.md"
    ],
    repo = "https://gitlab.com/leavehouse/TemporalMemory.jl/blob/{commit}{path}#L{line}"
)

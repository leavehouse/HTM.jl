# Hierarchical Temporal Memory

TODO: Some words go here that talk about the big picture.

Binary neurons, sparse activations, robustness to noise, etc.

## Temporal Memory

### Without learning

Without learning, a TM region doesn't do very much. It starts out with no distal synapses, and it doesn't grow any either.

Each TM region maintains, as part of its state, two distinct collections of cells, the **active** cells and the **winning** cells. These collections are initially empty, but when a collection of active input columns is received during an invocation of the main `compute!` function, the region updates its collection of active and winning cells.

If the TM region was performing learning, these active and winning cells would be used further in the learning algorithm. But since no learning is occurring, these collections are updated (during a `compute!` step) and then ignored until the next `compute!` step.

In this scenario, the pseudocode for `compute!` looks like this:

```
fn compute!(active_columns)
  empty the collections of active and winning cells
  for each column c
    if c in active_columns
      // activate unpredicted column c
      make all cells in c active // "burst" the column c
      make a random cell the sole winning cell of the column
    end
  end
end
```
